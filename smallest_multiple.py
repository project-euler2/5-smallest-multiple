def smallest_multiple(max):
    multiple = 0
    found = False
    while found == False :
        found = True
        multiple += 1
        for i in range(1,max):
            found = found and not(multiple % i)
    return multiple

print(smallest_multiple(20))